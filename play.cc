#include "playback.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

void rotate(cv::Mat& src, double angle, cv::Mat& dst) {
      int len = std::max(src.cols, src.rows);
      cv::Point2f pt(src.cols/2., src.rows/2.);
      cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
      cv::warpAffine(src, dst, r, cv::Size(src.cols, src.rows));
}

void printUsage()
{
   printf("\n  Usage: play input_oni_file \n\n"); 
}

int main(int argc, char* argv[]){
   Playback playback = Playback();
   char oni_filename[1000];

   if(argc < 2) {
      printUsage();
      return -1;
   }

   if (argc == 2 && strcmp(argv[1], "-h") == 0)
   {
      printUsage();
      return 0;
   }
   
   strcpy(oni_filename, argv[1]);
   playback.init(oni_filename);
   playback.player.SetPlaybackSpeed(XN_PLAYBACK_SPEED_FASTEST);
   cv::Mat out_img;
   printf("  oni: %s\n", oni_filename);

   XnUInt32 frames;
   playback.player.GetNumFrames(playback.g_image.GetName(), frames);
   printf("  Frames rgb: %i\n", frames);
   while (playback.update()) { 
      if(playback.frame%30 == 0)
         printf("  %d %lu\n", playback.frame, playback.timestamp);
      playback.depth.convertTo(out_img, CV_8UC1, 0.05f);
      imshow("rgb", playback.rgb);
      imshow("depth", out_img);
      char c = cv::waitKey(5);
      if(c == 27)
         break;
   }
   return 0;
}
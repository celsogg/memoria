CXX := g++ -w -ggdb `pkg-config --cflags opencv`
INCLUDES := -I. -I/usr/include/ni -I/usr/local/include -I/opt/local/include
LIBS := -L/usr/local/lib/ -lopencv_video -lopencv_highgui -lopencv_imgproc -lopencv_core -lopencv_videostab -L/usr/lib/ -lOpenNI `pkg-config --libs opencv`

all: playback.o oni_rgb_to_video play play_video mouse_test water people_detection bs kalman_mouse oni_rgb_to_images
	
# split: split.cpp playback.o 
# 	$(CXX) split.cpp playback.o $(INCLUDES) -o split $(LIBS) 

# record: record.cpp
# 	$(CXX) record.cpp $(INCLUDES) -o record $(LIBS) 

# preview: preview.cpp playback.o
# 	$(CXX) preview.cpp playback.o $(INCLUDES) -o preview $(LIBS)

playback.o: playback.cpp
	$(CXX) playback.cpp -c $(INCLUDES) -o playback.o $(LIBS)

oni_rgb_to_video: oni_rgb_to_video.cc playback.o
	$(CXX) oni_rgb_to_video.cc playback.o $(INCLUDES) -o oni_rgb_to_video $(LIBS)

play: play.cc playback.o
	$(CXX) play.cc playback.o $(INCLUDES) -o play $(LIBS)

play_video: play_video.cc
	$(CXX) play_video.cc $(INCLUDES) -o play_video $(LIBS)

test: test.cc
	$(CXX) test.cc $(INCLUDES) -o test $(LIBS)	

mouse_test: mouse_test.cc
	$(CXX) mouse_test.cc $(INCLUDES) -o mouse_test $(LIBS)

water: water.cc
	$(CXX) water.cc $(INCLUDES) -o water $(LIBS)

people_detection: people_detection.cc
	$(CXX) people_detection.cc $(INCLUDES) -o people_detection $(LIBS)

#bs: bs.cc
#	$(CXX) bs.cc $(INCLUDES) -o bs $(LIBS)

kalman_mouse: kalman_mouse.cc
	$(CXX) kalman_mouse.cc $(INCLUDES) -o kalman_mouse $(LIBS)

oni_rgb_to_images: oni_rgb_to_images.cc playback.o
	$(CXX) oni_rgb_to_images.cc playback.o $(INCLUDES) -o oni_rgb_to_images $(LIBS)

clean:
	rm -f rgb_to_video play play_video playback.o test mouse_test water people_detection bs kalman_mouse oni_rgb_to_images

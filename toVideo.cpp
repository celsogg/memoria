#include "playback.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/**
 * Rotate an image
 */
void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
    int len = std::max(src.cols, src.rows);
    cv::Point2f pt(src.cols/2., src.rows/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

    cv::warpAffine(src, dst, r, cv::Size(src.cols, src.rows));
}

int main(int argc, char* argv[])
{
  KinectPlayback playback = KinectPlayback();
  const char* oni_filename = "";

  if(argc < 2) {
    printf("Error - not enough arguments!\n");  
    return -1;
  }
  
  oni_filename = argv[1];
 
  playback.init(oni_filename);
  //playback.player.SetPlaybackSpeed(XN_PLAYBACK_SPEED_FASTEST);
  playback.player.SetPlaybackSpeed(0);
  Mat out_img;


  //C++: VideoWriter::VideoWriter(const string& filename, int fourcc, double fps, Size frameSize, bool isColor=true)
  //VideoWriter vw = VideoWriter("out.mpg",  CV_FOURCC('M','P','G','4'), playback.fps, Size(playback.cols, playback.rows), true);
  VideoWriter vw = VideoWriter();
  vw.open("out.avi",  CV_FOURCC('P','I','M','1'), playback.fps, Size(playback.cols, playback.rows), true);
  if ( ! vw.isOpened() )
  {
    printf("No se abrio\n");
  }
  else
  {
    printf("se abrio\n");
  }
  // Prevent from looping on itself.
  while (playback.update()) { 
    playback.depth.convertTo(out_img, CV_8UC1, 0.05f);
    
    //imshow("rgb", playback.rgb);
    imshow("depth", out_img);

    //if(playback.frame%30 == 0)
    //  printf("%d %lu\n", playback.frame, playback.timestamp);

    Mat rotated = playback.rgb.clone();
    rotate(playback.rgb, 110, rotated);
    //imshow("rotated", rotated);
    vw.write(rotated);

    char c = waitKey(5);
    if(c == 27)
      break;
  }

  return 1;
}

#include "playback.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
      int len = std::max(src.cols, src.rows);
      cv::Point2f pt(src.cols/2., src.rows/2.);
      cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

      cv::warpAffine(src, dst, r, cv::Size(src.cols, src.rows));
}

int main(int argc, char* argv[])
{
   Playback playback = Playback();
   char oni_filename[1000] = "";
   char output_video_file[1000] = "";

   if(argc < 3) {
      printf("Error - not enough arguments!\n ./rgb_to_video <input>.oni <output>.avi\n");  
      return -1;
   }
   
   strcpy(oni_filename, argv[1]);
   strcpy(output_video_file, argv[2]);
 
   playback.init(oni_filename);
   //playback.player.SetPlaybackSpeed(XN_PLAYBACK_SPEED_FASTEST);
   playback.player.SetPlaybackSpeed(XN_PLAYBACK_SPEED_FASTEST);
   cv::Mat out_img;

   printf("oni: %s\n", oni_filename);
   printf("avi: %s\n", output_video_file);

   //C++: VideoWriter::VideoWriter(const string& filename, int fourcc, double fps, Size frameSize, bool isColor=true)
   //VideoWriter vw = VideoWriter("out.mpg",  CV_FOURCC('M','P','G','4'), playback.fps, Size(playback.cols, playback.rows), true);
   cv::VideoWriter vw = cv::VideoWriter();
   vw.open(output_video_file,  CV_FOURCC('m','p','g','2'), playback.fps, cv::Size(playback.cols, playback.rows), true);
   
   if ( ! vw.isOpened() ) {
      printf("Writer not opened\n");
      return -1;
   }

   XnUInt32 frames;
   //GetNumFrames (const XnChar *strNodeName, XnUInt32 &nFrames)
   playback.player.GetNumFrames(playback.g_image.GetName(), frames);
   printf("total frames %i\n", frames);

   // Prevent from looping on itself.
   while (playback.update()) { 
      //playback.depth.convertTo(out_img, CV_8UC1, 0.05f);
      
      //imshow("rgb", playback.rgb);
      //imshow("depth", out_img);

      if(playback.frame%60 == 0)
        printf("%d %lu\n", playback.frame, playback.timestamp);

      cv::Mat rotated = playback.rgb.clone();
      rotate(playback.rgb, 180, rotated);
      //imshow("rotated", rotated);
      vw.write(rotated);

      //char c = waitKey(5);
      //if(c == 27)
      //  break;
   }

   return 0;
}

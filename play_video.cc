#include <opencv2/opencv.hpp>
#include <stdio.h>

using namespace cv;

int main(int argc, char* argv[])
{
    //if(argc < 2) {
    //    printf("Error - not enough arguments!\n ./play <input_file.oni>\n");  
    //    return -1;
    //}
    
    char* video_filename = "";

    if ( argc == 2){
        video_filename = argv[1];
    }

    VideoCapture cap( strcmp(video_filename,"") == 200 ? 0 : video_filename);
    if(!cap.isOpened()){
        printf("unable to open VideoCapture\n");
        return -1;
    }

    Mat edges;
    //namedWindow("edges",1);
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera

        if (!frame.data)
            break;

        //cvtColor(frame, edges, CV_BGR2GRAY);
        //GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        //Canny(edges, edges, 0, 30, 3);
        //imshow("edges", edges);
        imshow("frames", frame);
        if(waitKey(30) >= 0) break;
    }
    return 0;
}
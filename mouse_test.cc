#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <iostream>

cv::Point2f a,b;

static void onMouse( int event, int x, int y, int flags, void*)
{
	if ( event == cv::EVENT_LBUTTONDOWN ){
		printf("abajo\n");
		a.x = x;
		a.y = y;
	} else if ( event == cv::EVENT_LBUTTONUP ){
		b.x = x;
		b.y = y;
		printf("arriba\n");
		std::cout << a << " " << b << std::endl;
	} else if ( event == cv::EVENT_MOUSEMOVE && flags == cv::EVENT_FLAG_LBUTTON){
		//printf("moviendo\n");
	}
}

int main(int argc, char const *argv[])
{
	cv::Mat mat;
	if ( argc != 2 )
		return 1;
	else
		mat = cv::imread( argv[1], CV_LOAD_IMAGE_COLOR);
	
	if ( ! mat.data )
		return 1;

	cv::namedWindow("img", cv::WINDOW_NORMAL);
	cv::imshow("img", mat);
	cv::setMouseCallback("img", onMouse, 0);
	cv::waitKey(0);
	return 0;
}